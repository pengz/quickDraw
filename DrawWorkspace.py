# import ROOT
from ROOT import TFile
from ROOT import TH1D
from ROOT import TF1                                                                                                                    
from ROOT import TGraph                                                                                                                 
from ROOT import TCanvas    

from ROOT import RooFit    



def configCheck(obj):
    if obj==None:
        print("Initialize error on %s"%(obj.GetName()))
    # else:
    #     print("%s is ok"%(obj.GetName()))

def fullName(lst,tag):
    for name in lst:
        if tag in name:
            return name


class DrawWorkspace(object):

    def __init__(self):
        self.FileName        = ''
        self.WorkspaceName   = 'combWS'
        self.DataSetName     = 'combData'
        self.ConfigName      = 'ModelConfig'
        self.PlotfileName    = 'workspace.pdf'
        self.rebin           = 1  
        self.sigPdfName      = ''
        self.bkgPdfName      = ''
        self.modelPdfName    = ''


    def Prepare(self):   
        print("input File          : %s"%self.FileName     )
        print("input WorkspaceName : %s"%self.WorkspaceName)
        print("input DataSet       : %s"%self.DataSetName  )
        print("input ModelConfig   : %s"%self.ConfigName   )
        print("input rebin         : %s"%self.rebin      ) 

        self.File = TFile.Open(self.FileName ,"read")
        self.WS   = self.File.Get(self.WorkspaceName)
        self.data = self.WS.data(self.DataSetName)
        self.config = self.WS.obj(self.ConfigName)

        print("----- config check -----")
        configCheck(self.File)
        configCheck(self.WS)
        configCheck(self.data)
        configCheck(self.config)
        print("----- config is ok -----")

    def GetPdfName(self, ipdf):
        ComponentNameLst= [ pdf.GetName() for pdf in ipdf.getComponents() ]
        # fname=[ pdf.GetName() for pdf in ipdf.getComponents() if 'pdf__' in pdf.GetName() ]

        # Attention: return the index of first one in list
        ## Run3 hyy xs : '_model_Hyy_c0', '_modelSB_Hyy_c0', 'pdf__commonSig_Hyy_c0','pdf__background_Hyy_c0'
        # ComponentNameLst.index('pdf__commonSig_Hyy_c0')

        self.sigPdfName      = fullName(ComponentNameLst,'pdf__commonSig_')
        self.bkgPdfName      = fullName(ComponentNameLst,'pdf__background_')
        self.modelPdfName    = fullName(ComponentNameLst,'_model_')
        if self.modelPdfName == None:
            print(ComponentNameLst)

        print("----- pdf name : %s"%self.sigPdfName)
        print("----- pdf name : %s"%self.bkgPdfName)
        print("----- pdf name : %s"%self.modelPdfName)





    def Draw(self):

        pdf=self.config.GetPdf()
        cat=pdf.indexCat()

        datalist = self.data.split(cat,True)
        numChannels = cat.numBins('category')

        
        canvas = TCanvas("canvas","",800,600)
        canvas.cd()
        canvas.Print(self.PlotfileName+"[")

        for i in range(numChannels):
            cat.setBin(i)

            channelname=cat.getLabel()
            pdf_i = pdf.getPdf(channelname)

            data_i = datalist.At(i)

            x = pdf_i.getObservables(data_i).first()
            Observable_name = x.GetName()
            #Observable_name=config.GetObservables()[0].GetName()
            #x = pdf_i.getObservables(Observable_name)

            # rebin
            obsNBins = int(x.numBins()/self.rebin)
            print("Final obsNBins = %d"%obsNBins)


            ### loop over to read data            
            # dataHist=TH1D("dataHist","", obsNBins,x.getMin(),x.getMax());

            # print("begin c%02d: %s ==> %s"%(i,channelname,Observable_name))
            # for iEvt in range(data_i.numEntries()):
            #     if iEvt%10000==0:
            #         print("process c%02d: %d"%(i,iEvt))
            #     eventVar = data_i.get(iEvt)
            #     dataHist.Fill(eventVar.find(Observable_name).getVal(),data_i.weight())

            xframe = x.frame()

            x.setBins( obsNBins )

            from ROOT import RooAbsData

            data_i.plotOn(xframe, RooFit.Binning(obsNBins),RooFit.DataError(RooAbsData.Poisson),RooFit.MarkerSize(1.0),RooFit.Name("data"))

            from ROOT import kRed,kGreen,kBlue,kCyan,kWhite

            self.GetPdfName(pdf_i)

            pdf_i.plotOn(xframe, RooFit.Components(self.sigPdfName), RooFit.LineColor(kRed), RooFit.LineStyle(2), RooFit.Name("Signal"))
            pdf_i.plotOn(xframe, RooFit.Components(self.bkgPdfName), RooFit.LineColor(kGreen), RooFit.LineStyle(2), RooFit.Name("Background"))
            pdf_i.plotOn(xframe, RooFit.Components(self.modelPdfName), RooFit.LineColor(kBlue), RooFit.LineStyle(1), RooFit.Name("Total"))
            # pdf_i.plotOn(xframe, RooFit.Components('_model_Hyy_c0'), RooFit.Name("Total"))

            ## test 
            # lst=pdf_i.getComponents()

            # print("!!! %d"%len(lst))
            # for ii in range(len(lst)):
            #     # if ii==0:
            #     #     continue
            #     lst[ii].Print()
            #     print("!!! %d"%ii)

            # sig=lst[2]
            # bkg=lst[210]
            # sig.plotOn(xframe)
            # bkg.plotOn(xframe)

            ## test

            xframe.GetXaxis().SetTitle("m_{#gamma#gamma} GeV")
            xframe.GetXaxis().CenterTitle()
            xframe.GetYaxis().CenterTitle()
            xframe.GetYaxis().SetTitleOffset(1.1)

            xframe.Draw()
            
            from ROOT import TLegend
            legend = TLegend(0.53, 0.63, 0.86, 0.87)
            # legend.AddText(s_chi)
            legend.SetBorderSize(0)
            legend.SetLineColor(0)
            legend.SetLineStyle(1)
            legend.SetLineWidth(1)
            legend.SetFillColor(0)
            legend.SetFillStyle(0)
            legend.SetTextSize(0.05)

            legend.AddEntry("Signal"    , "Signal"    , "L")
            legend.AddEntry("Background", "Background", "L")
            legend.AddEntry("Total"     , "Total"     , "L")
            legend.AddEntry("data"      , "data"      , "P")
            legend.Draw('same')

            # dataHist = xframe.getHist("data")
            # dataHist.Draw("e1")

            canvas.Print(self.PlotfileName)


        #config.GetObservables()[0].GetName()
        #aa = [ an.GetName() for an in pdf_i.getVariables()]


        canvas.Print(self.PlotfileName+"]")





if __name__ == '__main__':
    #main()
    print("DrawWorkspace running")
else:
    print("Import DrawWorkspace")