import sys
import os
import argparse

# from module import class 
from DrawWorkspace import DrawWorkspace

parser = argparse.ArgumentParser(description='draw RooDataSet in workspace, -i -d ')
parser.add_argument('-i', '--input', action='store', required=True, help='path of Root file')
parser.add_argument('-w', '--workspace' , action='store', required=False, default='combWS', help='Name of workspace')
parser.add_argument('-d', '--dataset'   , action='store', required=False, default='combData', help='Name of dataset')
parser.add_argument('-m', '--modelconfig',action='store', required=False, default='ModelConfig', help='Name of modelconfig')
parser.add_argument('-n', '--name',action='store', required=False, default='workspace.pdf', help='Name of output')
parser.add_argument('-r', '--rebin',action='store', required=False, default=1, help='newNbin=oldNbin/rebin')

## Run3 hyy xs : '_model_Hyy_c0', '_modelSB_Hyy_c0', 'pdf__commonSig_Hyy_c0','pdf__background_Hyy_c0'
## mass combination: _model_Hyy_Conv_Cen_High_channel_run2_yy, pdf__commonSig_Hyy_Conv_Cen_High_channel_run2_yy, pdf__background_Hyy_Conv_Cen_High_channel_run2_yy
## the following three arguments are still testing
parser.add_argument('-s', '--sigPdfName',action='store', required=False, default='' , help='name of signal pdf in workspace')
parser.add_argument('-b', '--bkgPdfName',action='store', required=False, default='', help='name of background pdf in workspace')


args = parser.parse_args()


###

wsPlot = DrawWorkspace()

wsPlot.FileName      = '%s'%args.input
wsPlot.WorkspaceName = '%s'%args.workspace
wsPlot.DataSetName   = '%s'%args.dataset
wsPlot.ConfigName    = '%s'%args.modelconfig
wsPlot.PlotfileName  = '%s'%args.name
wsPlot.rebin         = int(args.rebin)

wsPlot.sigPdfName    = '%s'%args.sigPdfName
wsPlot.bkgPdfName    = '%s'%args.bkgPdfName


wsPlot.Prepare()
wsPlot.Draw()
