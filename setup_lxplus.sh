#!/bin/bash
setupATLAS

# Setup software
#lsetup "views LCG_101 x86_64-centos7-gcc8-opt"
lsetup "views LCG_102 x86_64-centos7-gcc8-opt"
#source /cvmfs/sft.cern.ch/lcg/releases/LCG_96b/CMake/3.14.3/x86_64-centos7-gcc8-opt/CMake-env.sh
#source /cvmfs/sft.cern.ch/lcg/releases/LCG_97_ATLAS_1/ROOT/v6.20.02/x86_64-centos7-gcc8-opt/ROOT-env.sh
#source /cvmfs/sft.cern.ch/lcg/releases/LCG_96b/Boost/1.70.0/x86_64-centos7-gcc8-opt/Boost-env.sh

# More memory
ulimit -S -s unlimited
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/afs/cern.ch/work/p/pengz/public/lcg102/quickFit/lib/
export RooFitExtensions_DIR=/afs/cern.ch/work/p/pengz/public/lcg102/quickFit/RooFitExtensions/
#export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/afs/cern.ch/work/g/glu/public/xmlAnaWSBuilder_comb4l/quickFit/lib/
#export RooFitExtensions_DIR=/afs/cern.ch/work/g/glu/public/xmlAnaWSBuilder_comb4l/quickFit/RooFitExtensions/
#export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/afs/cern.ch/work/g/glu/public/xmlAnaWSBuilder_updated/quickFit_2020/RooFitExtensions
